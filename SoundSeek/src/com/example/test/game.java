package com.example.test;




import java.util.Random;



import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;


public class game extends Activity implements OnItemClickListener{
	Integer rowbases=null,loops=0;
	ImageView imageView, firstView;
	MediaPlayer sp;
	int Click;
	int score=0;
	String res  = "";
	TextView tvTimer;
	
	 Integer[] ar = { R.drawable.one, R.drawable.two,R.drawable.three, R.drawable.four, R.drawable.five, R.drawable.six,R.drawable.seven,R.drawable.eight,R.drawable.nine,R.drawable.ten,R.drawable.eleven ,R.drawable.twel };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        
tvTimer = (TextView)findViewById(R.id.tvTimer);
        
        
        CountDownTimer cdt = new CountDownTimer(30000, 50) {
            public void onTick(long millisUntilFinished) {
            	String strTime = String.format("%.1f" , (double)millisUntilFinished / 1000);
                tvTimer.setText(String.valueOf(strTime));    
            }

            public void onFinish() {
            	 
            	
            	
            	 Intent goEnd= new Intent(game.this,last.class);
            	 goEnd.putExtra("sendscore", score);
            	 startActivity(goEnd);
            }
            
        
        }.start();
        
        
 
    
        android.widget.GridView gridview = (android.widget.GridView) findViewById(R.id.gridview1);
        gridview.setVerticalSpacing ( 5 );
        gridview.setHorizontalSpacing ( 5 );
        gridview.setAdapter(new ImageAdapter(this));
        gridview.setOnItemClickListener(this); 
        
        
        rand();
            
        
    }

 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public class ImageAdapter extends BaseAdapter
	{
	private Context context;
	public ImageAdapter(Context c)
	{
	// TODO Auto-generated method stub
	context = c;
	}
	public int getCount() {
	
	// TODO Auto-generated method stub
	
	return ar.length;
	
	}
	
	 
	
	public Object getItem(int position) {
	
	// TODO Auto-generated method stub

	return position;
	
	}
	
	public long getItemId(int position) {
	// TODO Auto-generated method stub
	return position;
	}
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView imageView;
		//imageView
		if (convertView == null) {
		imageView = new ImageView(context);
		imageView.setLayoutParams(new GridView.LayoutParams(200, 130));
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setPadding(4, 4, 4, 4);
		} else {
			imageView = (ImageView) convertView;
				}
		imageView.setImageResource(ar[position]);
		return imageView;
		}
		}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
		// TODO Auto-generated method stub
                     
        Click = position;
       /* if (Click==0){
        TextView tv = (TextView)findViewById(R.id.textView1);
        tv.setText("Dog!!!");
        }*/
        if(Click==rowbases){
        	
        	score++;
        }
       
       
        	rand();
        	//TextView tv = (TextView)findViewById(R.id.textView1);
       	
           // tv.setText(res);
            TextView tv2 = (TextView)findViewById(R.id.tvScore);
        	res =  String.valueOf(score);
            tv2.setText(res);
        
	}
	
	public void rand() {
		// TODO Auto-generated method stub
		Random random = new Random();
    	rowbases=random.nextInt(10);
    	 
    	if (rowbases==0)
		{
    		
    		playSound(game.this, R.raw.one);
			
		}
		else if (rowbases==1)
		{
			playSound(game.this, R.raw.two);
		}
		else if (rowbases==2)
		{
			playSound(game.this, R.raw.three);
		}
		else if (rowbases==3)
		{
			playSound(game.this, R.raw.four);
		}
		else if (rowbases==4)
		{
			playSound(game.this, R.raw.five);
		}
		else if (rowbases==5)
		{
			playSound(game.this, R.raw.six);
		}
		else if (rowbases==6)
		{
			playSound(game.this, R.raw.seven);
		}
		else if (rowbases==7)
		{
			playSound(game.this, R.raw.eight);
		}
		else if (rowbases==8)
		{
			playSound(game.this, R.raw.nine);
		}
		else if (rowbases==9)
		{
			playSound(game.this, R.raw.ten);
		}
		else if (rowbases==10)
		{
			playSound(game.this, R.raw.eleven);
		}
		
		else if (rowbases==11)
		{
			playSound(game.this, R.raw.twel);
		}
		
	
	}
	 public void onStop() {
	        super.onStop();
	        try {
	            if(sp != null && sp.isPlaying()) {
	                sp.stop();
	                sp.release();
	            }
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    public void playSound(Context context, int resId) {
	        try {
	            if(sp != null && sp.isPlaying()) {
	                sp.stop();
	                sp.release();
	            }
	        } catch (IllegalStateException e) {
	            e.printStackTrace();
	        }
	        sp = MediaPlayer.create(context, resId);
	        sp.start();
	        sp.setOnCompletionListener(new OnCompletionListener() {
	            public void onCompletion(MediaPlayer mp) {
	                mp.release();
	            }
	        });
	    }
	
	
}


